<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Model;
use App\Task;
use Faker\Generator as Faker;

$factory->define(App\Task::class, function (Faker $faker) {
    return array('title' => $faker->userName,
        'is_done' => $faker->numberBetween($min = 0, $max = 1),
        'user_id' => $faker->numberBetween($min = 1, $max = 10),
        'point' => $faker->numberBetween($min = 1, $max = 10)
    );
});

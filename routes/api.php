<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('task/list', 'TaskController@getAllTasks');

Route::POST('task', 'TaskController@saveTask');

Route::get('task/{id}', 'TaskController@getTask');

Route::put('task/{id}', 'TaskController@updateTask');

Route::get('user/list', 'UserController@getAllUser');



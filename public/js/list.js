function doneTask(data) {
    var myData = {
        "id": data.id,
        "parent": data.parent_id,
        "userID": data.user_id,
        "title": data.title,
        "points": data.point,
        "isDone": 1,
    }
    $.ajax({
        type: 'PUT',
        url: baseUrl + 'api/task/' + data.id,
        data: myData,
        success: function (data) {
            $("#success").html("Task Update Successfull");
            $("#success").css("display", "block");
            location.reload();
        },
        error: function (data) {
            $("#error").html(data.responseJSON.message);
            $("#error").css("display", "block");
        },
        beforeSend: function () {

        }
    });
}

function notDoneYetTask(data) {
    var myData = {
        "id": data.id,
        "parent": data.parent_id,
        "userID": data.user_id,
        "title": data.title,
        "points": data.point,
        "isDone": 0,
    }
    $.ajax({
        type: 'PUT',
        url: baseUrl + 'api/task/' + data.id,
        data: myData,
        success: function (data) {
            $("#success").html("Task Update Successfull");
            $("#success").css("display", "block");
            location.reload();
        },
        error: function (data) {
            $("#error").html(data.responseJSON.message);
            $("#error").css("display", "block");
        },
        beforeSend: function () {

        }
    });
}
function saveTask() {
    $("#success").css("display", "none");
    $("#error").css("display", "none");
    isDone = 0;
    parent = $("#parent").val();
    userID = $("#user").val();
    title = $("#title").val();
    points = $("#point").val();
    if ($('#yesbutton').is(':checked'))
        isDone = 1;

    var myData = {
        "parent": parent,
        "userID": userID,
        "title": title,
        "points": points,
        "isDone": isDone
    }
    $.ajax({
        type: 'POST',
        url: baseUrl + 'api/task',
        data: myData,
        success: function (data) {
            $("#success").html("Task Create Successfull");
            $("#success").css("display", "block");
        },
        error: function (data) {
            $("#error").html(data.responseJSON.message);
            $("#error").css("display", "block");
        },
        beforeSend: function () {

        }
    });
}
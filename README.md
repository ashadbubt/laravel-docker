### Installation

Dillinger requires composer to install dependencies.

Go to application directory and open terminal.
Install the dependencies and devDependencies 
```sh
$ composer install
```

and start the server.

```sh
$ cd ./.docker
$ docker-compose build
$ docker-compose up
```
Enable apache rewrite mode module
```sh
$ docker-compose exec app a2nmod rewrite
$ docker-compose exec app service apache2 restart
```
Start Server again.
Run the migration.

```sh
$ docker-compose exec app php artisan migrate
```
To run unit test 
```sh
$ docker-compose exec app php ./vendor/bin/phpunit
```
### Usage
Open the bellow link for use application
http://localhost:8080/

For API documentation Import the following postman URL
[Postman](https://www.getpostman.com/collections/ec344db95b744385f3bf)
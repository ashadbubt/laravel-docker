<?php

namespace App\Http\Controllers;

use App\Repository\Task\TaskInterface;

use Illuminate\Http\Request;
/**
 * This is an Home controller fromtheir we show our home page list
 * and model
 *  *
 * @category   Class
 * @package
 * @author     Ashad Zaman <ashad.feni@gamil.com>
 * @version    1.0.0
 */
class HomeController extends UserController
{
    /**
     * An object of task
     *
     * @var object
     */
    private $task;

    public function __construct(TaskInterface $task)
    {
        $this->task = $task;
    }
    /**
     * This function view home page , befor show home page it gather user info and all task
     * @name  homePage
     * @return  void
     * @see        task::getAll(),
     * @see        self::getAllUser(),
     * */
    public function homePage()
    {
        $users = $this->getAllUser();
        $tasks = $this->task->getAll();
        return view('welcome')
        ->with(compact('users'))
        ->with(compact('tasks'));
    }
    /**
     * This function view list page , befor show list page it gather user info and all task
     * @name  showLists
     * @return  void
     * @see        task::getAll(),
     * @see        self::getAllUser(),
     * @see        task::getUserTask(),
     * @see        self::buildTree(),
     * @see        task::getUserCompltePoint(),
     * @see        task::getUserTotalPoint(),
     * */
    public function showList()
    {
        $tasks = $this->task->getAll();
        $usersWithTask = $this->getAllUser();
        foreach ($usersWithTask as $key => $value) {
            $userTasks = $this->task->getUserTask($value->id);
            $usersWithTask[$key]->task =$this->buildTree($userTasks);
            $usersWithTask[$key]->completeTask = $this->task->getUserCompltePoint($value->id);
            $usersWithTask[$key]->totalTask = $this->task->getUserTotalPoint($value->id);
        }
        // return $usersWithTask;
        return view('list')
        ->with(compact('tasks'))
        ->with(compact('usersWithTask'));
    }
    /**
     * This function use for build tree for recursibly print from a flat print to view
     * @name  buildTree
     * @return   array
     * @see        self::buildTree()
     * */
    public function buildTree(array &$elements, $parentId = 0)
    {
        $branch = array();
        foreach ($elements as &$element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[$element['id']] = $element;
                unset($element);
            }
        }
        return $branch;
    }
    
}

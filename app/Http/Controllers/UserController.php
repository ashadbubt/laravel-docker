<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

/**
 * This Controller use for get user list form out site by request url
 * @category   Class
 * @package
 * @author     Ashad Zaman <ashad.feni@gamil.com>
 * @version    1.0.0
 */
class UserController extends Controller
{
       
    public function __construct()
    {
       
    }
     /**
     * IT request given url then reurn to controller after make json. use guzzle clint for request
     * @name  getAllUser
     * @return  json
     * */
    public function getAllUser()
    {
        $client = new Client();
        $response = $client->request('GET', 'https://gitlab.iterato.lt/snippets/3/raw');
        $response->getStatusCode();
        $response->getHeaderLine('content-type');
        $jsonRes = json_decode($response->getBody());
        return $jsonRes->data;
    }
}

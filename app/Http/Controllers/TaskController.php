<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\Task\TaskInterface;
use Response;
use Validator;
use Exception;
/**
 * This Task Controller its communicate to oure repository task for communicate database
 * Its mange all task related work
 * @category   Class
 * @package
 * @author     Ashad Zaman <ashad.feni@gamil.com>
 * @version    1.0.0
 */

class TaskController extends Controller
{
    /**
     * An object of task
     *
     * @var object
     */
    private $task;
    
    public function __construct(TaskInterface $task)
    {
        $this->task = $task;
    }
    /**
     * This funciton use for get all task task from model
     * @name  getAllTasks
     * @return  object
     * @see     task::getAll(),
     * */
    public function getAllTasks()
    {
        return $this->task->getAll();
    }
    /**
     * This Use for save a task , its get task data from request then validate and request to save database
     * @name  saveTask
     * @return  json
     * @see        task::checkLavelExceded(),
     * @see        task::create(),
     *
     */
    public function saveTask(Request $request)
    {
        $depth = 1;
        $rules = array(
            'userID' => 'required',
            'points' => 'required|integer|min:1|max:10',
            'isDone' => 'required|integer|min:0|max:15',
            'title' => 'required'
        );
        $messages = [
            'userID.required' => 'User is required!',
            'points.required' => 'Point is required!',
            'points.min'      => 'Point minimum Value 1!',
            'points.max'      => 'Point Maximum Value 10!',
            'isDone.required' => 'Done Or Not is required!',
            'isDone.min'      => 'Provide Invlid Is Done or Not ',
            'isDone.max'      => 'Provide Invlid Is Done or Not ',
            'title.required'  => 'Title is required',
        ];

        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            $errorMsgString = implode("<br/>", $validation->messages()->all());
            return Response::json(array('success' => false, 'heading' => 'Validation Error', 'message' => $errorMsgString), 400);
        }
        
        $depth = $this->task->checkLavelExceded($request->parent);
        // var_dump($depth);
        if ( $depth === false ) {
            return Response::json(array('success' => false, 'heading' => 'Validation Error', 'message' => "Exceed Depth lavel"), 400);
        }

        try {
            $data = array(
            'user_id'=> $request->userID,
            'point'=> intval($request->points),
            'is_done'=> intval($request->isDone),
            'title'=> $request->title,
            'parent_id'=> $request->parent,
            'depth'=>  $depth
             );
            $task = $this->task->create($data);
            return Response::json($task, 201);
        } catch (Exception $e) {
            return Response::json('Failed To Create ', 500);
        }
    }
    /**
     * This function use for get tsk by task id
     * @name  getTask
     * @return  void
     * @see     task::getById(),
     * */
    public function getTask($id)
    {
        return $this->task->getById($id);
    }
    /**
     * This Use for update a task , its get task data from request then validate and request to save database
     * @name  saveTask
     * @return  json
     * @see        task::changeChildDoneStatus(),
     * @see        task::changeParentDoneStatus(),
     * @see        task::update()
     * @see        task::getById()
     *
     */
    public function updateTask(Request $request)
    {
        $depth = 1;
        $rules = array(
            'id' => 'required',
            'userID' => 'required',
            'points' => 'required|integer|min:1|max:10',
            'isDone' => 'required|integer|min:0|max:15',
            'title' => 'required'
        );
        $messages = [
            'id.required' => 'Task ID IS required !',
            'userID.required' => 'User is required!',
            'points.required' => 'Point is required!',
            'points.min'      => 'Point minimum Value 1!',
            'points.max'      => 'Point Maximum Value 10!',
            'isDone.required' => 'Done Or Not is required!',
            'isDone.min'      => 'Provide Invlid Is Done or Not ',
            'isDone.max'      => 'Provide Invlid Is Done or Not ',
            'title.required'  => 'Title is required',
        ];

        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            $errorMsgString = implode("<br/>", $validation->messages()->all());
            return Response::json(array('success' => false, 'heading' => 'Validation Error', 'message' => $errorMsgString), 400);
        }

        try {
            $data = array(
            'id' => $request->id,
            'user_id'=> $request->userID,
            'point'=> intval($request->points),
            'is_done'=> intval($request->isDone),
            'title'=> $request->title,
            'parent_id'=> $request->parent,
            'depth'=>  $depth
             );
            $taskPrevious = $this->task->getById($request->id);
            $task = $this->task->update( $request->id, $data);
            if ($taskPrevious->is_done != $task->is_done ) {
                $this->task->changeChildDoneStatus($request->id, $request->isDone);
                $this->task->changeParentDoneStatus($request->id, $request->isDone);
            }
            
            return Response::json($task, 201);
        } catch (Exception $e) {
            return Response::json('Update Task Failed ', 500);
        }
    }
     /**
     * Its calculate total task of user
     * @name  getUserComplteTask
     * @return  void
     * */
    public function getUserComplteTask($idUser)
    {
        $totalTaskComplete = 0;
        $allTaskUser = $this->taskModel::where('user_id', $idUser)->get();

        foreach ($allTaskUser as $key => $value) {
            if ( $value->parent_id == null && $value->is_done == 1 ) {
                $totalTaskComplete = $totalTaskComplete + $value->point;
            } else {
                $parentData = $this->getById($value->parent_id);
                if ( $parentData->is_done == 0 && $value->is_done == 1) {
                    $totalTaskComplete = $totalTaskComplete+$value->point;
                }
            }
        }
        return $totalTaskComplete;
    }
}

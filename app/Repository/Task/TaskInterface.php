<?php

namespace App\Repository\Task;

/**
 * This is an task interface use as middle man for task controller a
 * and model
 *  *
 * @category   Class
 * @package
 * @author     Ashad Zaman <ashad.feni@gamil.com>
 * @version    1.0.0
 */
interface TaskInterface
{
    /**
     * This function return all task to caller
     * @name  getAll
     * @return  array
     */
    public function getAll();

    /**
     * This want task id as paramenter then return task details to user
     * @name  getById
     * @return  object
     * @param   int //task id
     */
    public function getById($id);
    /**
     * This function use for create a task
     * @name  create
     * @return  object
     * @param   array
     */
    public function create(array $attributs);
    /**
     * This function use for update task
     * @name  update
     * @return  object
     * @param   array // update data
     * @param   int //id of task
     */
    public function update($id, array $attributs);
    /**
     * This function use for delete a task
     * @name  delete
     * @return  boolen
     *
     * @param   int //id of task
     */
    public function delete($id);
}
<?php

namespace App\Repository\Task;

use App\Task;
/**
 * This is an task class which use as middle man for task controller a
 * and model
 *  *
 * @category   Class
 * @package
 * @author     Ashad Zaman <ashad.feni@gamil.com>
 * @version    1.0.0
 */
class EloquentTask implements TaskInterface
{
    
    private $taskModel;

    public function __construct(Task $taskModel)
    {
        $this->taskModel = $taskModel;
    }
    /**
     * This function return all task to caller
     * @name  getAll
     * @return  array
     */
    public function getAll()
    {
        return $this->taskModel->all();
    }
    /**
     * This want task id as paramenter then return task details to user
     * @name  getById
     * @return  object
     * @param   int //task id
     */
    public function getById($id)
    {
        return $this->taskModel->find($id);
    }
    /**
     * This function use for create a task
     * @name  create
     * @return  object
     * @param   array
     */
    public function create(array $attributs)
    {
        $task = $this->taskModel->create($attributs);
        return $task;
    }
    /**
     * This function use for update task
     * @name  update
     * @return  object
     * @param   array // update data
     * @param   int //id of task
     */
    public function update($id, array $attributs)
    {
        $task = $this->taskModel->findOrFail($id);
        $task->update($attributs);
        return $task;
    }
    /**
     * This function use for delete a task
     * @name  delete
     * @return  boolen
     *
     * @param   int //id of task
     */
    public function delete($id)
    {
        $this->getById($id)->delete();
        return true;
    }
    /**
     * This function use for check dose task lavel exceed
     * of our requirement 5 , so its take a task id the check the lavel of task allredy insert
     * if lavel exceed retrun fasle other wise number ol lavel
     * @name  checkLavelExceded
     * @return  boolen/number
     *
     * @param   int //id of task
     */
    public function checkLavelExceded($parent)
    {
        if ( $parent === null || $parent === '') {
            return 1;
        }
        $task = $this->getById($parent);

        $depth = $task->depth;

        $depth= $depth + 1;
        if ($depth <= 5 ) {
            return $depth;
        } else {
            return false;
        }
       
    }
    /**
     * This function use for get all task assign to user id.
     * @name  getUserTask
     * @return  object
     *
     * @param   int //IdUsers
     */
    public function getUserTask($idUser)
    {
        $userTasks = $this->taskModel::where('user_id', $idUser)->get()->toArray();
        return $userTasks;
    }
    /**
     * This function calculate total task assign to a usser it  only calculate top parent value
     * because child value ar included to paren.
     * @name  getUserTotalPoint
     * @return  int
     *
     * @param   int //IdUsers
     */
    public function getUserTotalPoint($idUser)
    {
        $totalTaskValue = $this->taskModel::where('user_id', $idUser)->whereNull('parent_id')->sum('point');
        return $totalTaskValue;
    }
    /**
     * This function use for user completed task point . onlay point are count if completed task also
     * check if parent are completed then child should be not count, because child point are included to parent
     * @name  getUserCompltePoint
     * @return  int
     *
     * @param   int //IdUsers
     */
    public function getUserCompltePoint($idUser)
    {
        $totalTaskComplete = 0;
        $allTaskUser = $this->taskModel::where('user_id', $idUser)->get();

        foreach ($allTaskUser as $key => $value) {
            if ( $value->parent_id == null && $value->is_done == 1 ) {
                $totalTaskComplete = $totalTaskComplete + $value->point;
            } else {
                $parentData = $this->getById($value->parent_id);
                if ( $parentData ) {
                    if ( $parentData->is_done == 0 && $value->is_done == 1) {
                        $totalTaskComplete = $totalTaskComplete+$value->point;
                    }
                }
            }
        }
        return $totalTaskComplete;
    }
   /**
     * This function use for update child done status ,
     * because if parent marks as done all child should be done accourding to business
     * @name  changeChildDoneStatus
     * @return  int
     *
     * @param   int //idTask
     * @param   int //status
     */
    public function changeChildDoneStatus($idTask, $status)
    {
        $allChildTask = $this->taskModel::where('parent_id', $idTask)->get();
        foreach ($allChildTask as $key => $value) {
            $this->update($value->id, array('is_done'=>$status));
            $allChildTask = $this->taskModel::where('parent_id', $value->id)->get();
            if ($allChildTask ) {
                $this->changeChildDoneStatus($value->id, $status);
            }
        }
    }
    /**
     * This function use for update parent done status ,
     * because if child ore not done parent should be not done status,also if all child are done parent should be done
     * @name  changeChildDoneStatus
     * @return  int
     *
     * @param   int //idTask
     * @param   int //status
     */
    public function changeParentDoneStatus($idTask, $status)
    {
        if ( $status == 0 ) {
            $task = $this->getById($idTask);
            while ( $task->parent_id != null ) {
                $this->update($task->parent_id, array('is_done'=>$status));
                $task = $this->getById($task->parent_id);
            }
        } else {
            $task = $this->getById($idTask);
            while ( $task->parent_id != null ) {
                if ( $this->taskModel::where('parent_id', $task->parent_id)->where('is_done', 0)->notExists() ) {
                    $this->update($task->parent_id, array('is_done'=>$status));
                    $task = $this->getById($task->parent_id);
                } else {
                    $task->parent_id =null;
                }
            }
        }
    }

}



<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repository\Task\TaskInterface;

use App\Repository\Task\EloquentTask;



class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //this use for DI if anny one want to TaskInterface it will retern class of
        $this->app->singleton(TaskInterface::class, EloquentTask::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

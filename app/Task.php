<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * This is task model whic are related to database
 * and model
 *  *
 * @category   Class
 * @package
 * @author     Ashad Zaman <ashad.feni@gamil.com>
 * @version    1.0.0
 */
class Task extends Model
{
   //
    protected $fillable = ['user_id', 'title','parent_id','point','is_done','depth'];
}

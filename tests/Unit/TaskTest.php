<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Task;

/**
 * This is a test calss for unit testing of my model
 * @category   Class
 * @package
 * @author     Ashad Zaman <ashad.feni@gamil.com>
 * @version    1.0.0
 */
class TaskTest extends TestCase
{
    use  RefreshDatabase;
    
    /**
     * This funciton unitest dose my model can careate task
     * @name  testTaskCanBeCreated
     * @see Task::find
     */
    public function testTaskCanBeCreated()
    {
        $tasks = factory(\App\Task::class)->create();
        $foundTask = Task::find(1);
        $this->assertEquals($foundTask->title, $tasks->title);
    }
    /**
     * This funciton unitest dose my model can delete task
     * @name  testTaskCanBeDeleted
     * @see Task::find
     * @see Task::delete
     */
    public function testTaskCanBeDeleted()
    {
        $factoryTask = factory(\App\Task::class)->create();
        $task = Task::find($factoryTask->id);
        $task->delete();
        $this->assertDatabaseMissing('tasks', ['id'=>1]);
    }
    /**
     * This funciton unitest dose my model can delete task
     * @name  testTaskCanBeUpdated
     * @see Task::find
     * @see Task::update
     */
    public function testTaskCanBeUpdated()
    {
        $factoryTask = factory(\App\Task::class)->create();
        $task = Task::find($factoryTask->id);
        $task->update(["title"=>"ashad"]);
        $task = Task::find($factoryTask->id);
        $this->assertEquals($task->title, 'ashad');
    }
}

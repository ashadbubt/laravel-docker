<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <!-- <script src="/public/js/home.js"></script> -->
    </head>
    <body>       
           <div class="row">
                <div class="col-md-12">

                    <div class="col-md-2">
                    </div>                     
                    <div class="col-md-8">
                      <table class="table">
                          <thead>
                              <tr>
                                  <th>ID</th>
                                  <th>Title</th>
                                  <th>Point</th>
                                  <th>Is Done</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php
                            foreach ($tasks as $value) {
                                echo "<tr>";
                                echo "<td>".$value->id."</td>";
                                echo "<td>".$value->title."</td>";
                                echo "<td>".$value->point."</td>";
                                echo "<td>".$value->is_done."</td>";
                                if ( $value->is_done == 0 ) {
                                    echo "<td><a name=\"\" id=\"\" class=\"btn btn-primary\" href=\"#\" role=\"button\" onClick='doneTask(".$value.")' >Done</a></td>";
                                } else {
                                    echo "<td><a name=\"\" id=\"\" class=\"btn btn-danger\" href=\"#\" role=\"button\" onClick='notDoneYetTask(".$value.")' >Not Done Yet</a></td>";
                                }
                                 
                                 echo "</tr>";
                            }
                            ?>                     
                          </tbody>
                            
                          <tr>
                                  <td scope="row"></td>
                                  <td></td>
                                  <td></td>
                              </tr>
                      </table>
                    </div> 

                    <div class="col-md-2">
                    </div> 
                </div> 
           </div>
           <div class="col-md-12">

                <?php echo "<pre>";printAllUserData($usersWithTask) ?>                               
           </div>
    </body>
</html>
<?php
function printAllUserData($userWithTask)
{
    foreach ($userWithTask as $value) {
         echo "<div class = 'col-md-4'>";
         echo "<p>".$value->first_name.' '.$value->last_name.'('.$value->completeTask.'/'.$value->totalTask.')' ."</p>";
         printTask($value->task);
         echo "</div>";
    }
}


function printTask($children, $lavel = 1)
{
    $px = $lavel *15 ;
    foreach ($children as $v) {
        echo "<p style ='margin-left:".$px."px'>".$v["title"]."(".$v["point"].") </p>";
        if (array_key_exists("children", $v) ) {
            $lavel = $lavel+1;
            printTask($v["children"], $lavel);
        }
    }
}
?>
<script>
var baseUrl = "http://localhost:8080/index.php/"
</script>

<script src="/js/list.js"></script>
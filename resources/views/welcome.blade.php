<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <!-- <script src="/public/js/home.js"></script> -->
    </head>
    <body>       
           <div class="row">
                <div class="col-md-12">

                    <div class="col-md-2">
                    </div>                     
                    <div class="col-md-8">
                        <form>
                            <div class="form-group">                                
                              <label for="">User</label>                             
                                <select class="form-control" name="user" id="user">
                                    <?php foreach ($users as $value) {
                                        echo "<option value='".$value->id."'>".'('.$value->first_name.')'.$value->email."</option>";
                                    }
                                    ?>
                                </select>   
                            </div>
                            <div class="form-group">
                              <label for="">Parent</label>
                                <select class="form-control" name="parent" id="parent">
                                    <option value=''></option>;
                                    <?php foreach ($tasks as $value) {
                                        echo "<option value='".$value->id."'>".$value->title."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                              <label for="">Title</label>
                              <input type="text"  class="form-control" name="title" id="title"  placeholder="" aria-describedby="helpId">                            
                            </div>
                            <div class="form-group">
                              <label for="">Point</label>
                              <input type="number"  name="point" id="point" class="form-control" placeholder="" aria-describedby="helpId">                             
                            </div>
                            <div class="form-group">
                              <label for="">Is Done : </label>                 
                              <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="">
                                        <input type="radio" name="isDone" id="yesbutton" value='1' autocomplete="off" > Yes
                                    </label>
                                    <label >
                                        <input type="radio" name="isDone" id="noButton" value='0' autocomplete="off" checked> No
                                    </label>
                               </div>
                            </div>  
                            <div>
                                <a name="saveTask" id="saveTask" class="btn btn-warning"  href="#" role="button" onClick="saveTask()"> Save Data </a>
                                <a name="saveTask" id="saveTask" class="btn btn-default" target="_blanck"  href="http://localhost:8080/index.php/list" role="button" onClick="saveTask()"> Show List </a>                                                                
                            </div>    
                            <div id="message" class="display-1">
                                <div class="alert alert-danger" role="alert" id="error" style="display:none">
                                   
                                </div>
                                <div class="alert alert-success" role="alert" id="success" style="display:none">
                                   
                                </div>
                            </div>                                               
                        </form>
                    </div> 

                    <div class="col-md-2">
                    </div> 
                </div> 
           </div>
    </body>
</html>
<script>
var baseUrl = "http://localhost:8080/"
</script>

<script src="/js/home.js"></script>